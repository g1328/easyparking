from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.
class Usuario(models.Model):
    usuario= models.CharField(max_length=20)
    password=models.CharField(max_length=20)
    nombre=models.CharField(max_length=100)

class Tipo_vehiculo(models.Model):
    tipo= models.IntegerField()
    descripcion=models.CharField(max_length=20)
    nombre=models.CharField(max_length=100)
    
class Registro(models.Model):
    placa= models.CharField(max_length=10)
    tipo_vehiculo=models.IntegerField()
    entrada = models.DateTimeField()
    salida = models.DateTimeField()
    cobrado = models.IntegerField(null=True)
    usuario=models.ForeignKey(Usuario, on_delete=CASCADE, null=True)

    

    